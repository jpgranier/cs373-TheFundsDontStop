from backend.models.models import VotesmartBills
from flask_cors import cross_origin
from flask import request
from flask import jsonify
from flask import Blueprint
import json
from sqlalchemy import func
from sqlalchemy import or_

votesmart_bills_api = Blueprint(
    "votesmart_bills", __name__, template_folder="templates"
)


@votesmart_bills_api.route("/getVotesmartBill", methods=["GET"])
@cross_origin()
def get_votesmart_bill():
    votesmart_bill = VotesmartBills.query.filter_by(
        bill_id=request.args.get("bill_id")
    ).first()
    response = json.dumps(votesmart_bill.as_dict())
    return response, 200


@votesmart_bills_api.route("/getBillsByCategoryId")
@cross_origin()
def get_bills_by_category_id():
    votesmart_bills = VotesmartBills.query.filter_by(
        categories=request.args.get("category_id")
    ).all()
    response_dict = dict()
    response_dict["categoryId"] = request.args.get("category_id")
    response_dict["bills"] = [v.as_dict() for v in votesmart_bills]
    response = json.dumps(response_dict)
    return response, 200


@votesmart_bills_api.route("/getVotesmartBillRangeFilterSearchAndSort", methods=["GET"])
@cross_origin()
def get_votesmart_bill_range_filter_search_and_sort():
    offset = int(request.args.get("offset"))
    if offset < 1:
        return "", 400

    bill_number_prefix = request.args.get("bill_number_prefix")
    title_first_letter = request.args.get("title_first_letter")
    date_introduced = request.args.get("date_introduced")
    primary_sponsor_first_letter = request.args.get("primary_sponsor_first_letter")
    bill_type = request.args.get("bill_type")

    search = request.args.get("search")

    sort_column = request.args.get("sort_column")
    asc_or_desc = request.args.get("asc_or_desc")

    votesmart_bills_base_query = VotesmartBills.query

    # Filtering
    if bill_number_prefix is not None:
        votesmart_bills_base_query = votesmart_bills_base_query\
            .filter(VotesmartBills.bill_number.startswith(bill_number_prefix))
    if title_first_letter is not None:
        votesmart_bills_base_query = votesmart_bills_base_query\
            .filter(VotesmartBills.title.startswith(title_first_letter))
    if date_introduced is not None:
        votesmart_bills_base_query = votesmart_bills_base_query.filter_by(date_introduced=date_introduced)
    if primary_sponsor_first_letter is not None:
        votesmart_bills_base_query = votesmart_bills_base_query\
            .filter(VotesmartBills.primary_sponsor.startswith(primary_sponsor_first_letter))
    if bill_type is not None:
        votesmart_bills_base_query = votesmart_bills_base_query.filter_by(bill_type=bill_type)

    # Searching
    if search is not None:
        words = search.split()
        conditions = []
        for word in words:
            conditions.append(
                func.concat(
                    VotesmartBills.bill_number,
                    VotesmartBills.title,
                    VotesmartBills.date_introduced,
                    VotesmartBills.primary_sponsor,
                    VotesmartBills.bill_type,
                ).like("%" + word + "%")
            )
        votesmart_bills_base_query = votesmart_bills_base_query.filter(or_(*conditions))

    # Sorting
    if sort_column is not None and asc_or_desc is not None:
        if sort_column == "bill_number":
            if asc_or_desc == "asc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.bill_number.asc())
            elif asc_or_desc == "desc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.bill_number.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "title":
            if asc_or_desc == "asc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.title.asc())
            elif asc_or_desc == "desc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.title.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "date_introduced":
            if asc_or_desc == "asc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.date_introduced.asc())
            elif asc_or_desc == "desc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.date_introduced.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "primary_sponsor":
            if asc_or_desc == "asc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.primary_sponsor.asc())
            elif asc_or_desc == "desc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.primary_sponsor.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "bill_type":
            if asc_or_desc == "asc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.bill_type.asc())
            elif asc_or_desc == "desc":
                votesmart_bills_base_query = votesmart_bills_base_query.order_by(VotesmartBills.bill_type.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        else:
            return "Unknown sort_column, " + sort_column, 400

    response = dict()
    response["size"] = votesmart_bills_base_query.count()
    votesmart_bills = votesmart_bills_base_query.limit(10).offset(10 * offset - 10)
    response["data"] = [vb.as_dict() for vb in votesmart_bills]
    return jsonify(response), 200


@votesmart_bills_api.route("/getAllVotesmartBillBillTypes", methods=["GET"])
@cross_origin()
def get_all_votesmart_bill_bill_types():
    bill_types = []
    for votesmart_bill in (
        VotesmartBills.query.with_entities(VotesmartBills.bill_type)
        .order_by(VotesmartBills.bill_type.asc())
        .distinct()
    ):
        bill_types.append(votesmart_bill.bill_type)
    response = jsonify(bill_types)
    return response, 200


@votesmart_bills_api.route("/getAllVotesmartBills", methods=["GET"])
@cross_origin()
def get_all_votesmart_bills():
    votesmart_bills = VotesmartBills.query.all()
    response = [vb.as_dict() for vb in votesmart_bills]
    return jsonify(response), 200
