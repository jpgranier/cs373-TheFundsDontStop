from backend.models.models import InterestGroups
from flask_cors import cross_origin
from flask import request
from flask import jsonify
from flask import Blueprint
import json
from sqlalchemy import func
from sqlalchemy import or_

interest_groups_api = Blueprint(
    "interest_groups", __name__, template_folder="templates"
)


@interest_groups_api.route("/getInterestGroupsByCategoryId", methods=["GET"])
@cross_origin()
def get_interest_groups_by_category_id():
    interest_groups = InterestGroups.query.filter_by(
        category_id=request.args.get("category_id")
    ).all()
    response = json.dumps([ig.as_dict() for ig in interest_groups])
    return response, 200


@interest_groups_api.route("/getInterestGroupIds")
@cross_origin()
def get_interest_group_ids():
    interest_groups = InterestGroups.query.all()
    id_list = []
    for interest_group in interest_groups:
        id_list.append(
            {
                "sig_id": interest_group.sig_id,
                "votesmart_id": interest_group.votesmart_id,
                "sig_name": interest_group.name,
            }
        )
    response = jsonify(id_list)
    return response, 200


@interest_groups_api.route("/getInterestGroups", methods=["GET"])
@cross_origin()
def get_interest_groups():
    interest_groups = InterestGroups.query.filter_by(
        sig_id=request.args.get("sig_id")
    ).first()
    response = json.dumps(interest_groups.as_dict())
    return response, 200


@interest_groups_api.route("/getInterestGroupsByVotesmartId", methods=["GET"])
@cross_origin()
def get_interest_groups_by_votesmart_id():
    interest_groups = InterestGroups.query.filter_by(
        votesmart_id=request.args.get("votesmart_id")
    ).first()
    response = json.dumps(interest_groups.as_dict())
    return response, 200


@interest_groups_api.route("/getInterestGroupsRangeFilterSearchAndSort", methods=["GET"])
@cross_origin()
def get_interest_groups_range_filter_search_and_sort():
    offset = int(request.args.get("offset"))
    if offset < 1:
        return "", 400

    name_first_letter = request.args.get("name_first_letter")
    total_contributions_start = request.args.get("total_contributions_start")
    total_contributions_end = request.args.get("total_contributions_end")
    dem_contributions_start = request.args.get("dem_contributions_start")
    dem_contributions_end = request.args.get("dem_contributions_end")
    repub_contributions_start = request.args.get("repub_contributions_start")
    repub_contributions_end = request.args.get("repub_contributions_end")
    pac_contributions_start = request.args.get("pac_contributions_start")
    pac_contributions_end = request.args.get("pac_contributions_end")

    search = request.args.get("search")

    sort_column = request.args.get("sort_column")
    asc_or_desc = request.args.get("asc_or_desc")

    interest_groups_base_query = InterestGroups.query

    # Filtering
    if name_first_letter is not None:
        interest_groups_base_query = interest_groups_base_query\
            .filter(InterestGroups.name.startswith(name_first_letter))
    if total_contributions_start is not None and total_contributions_end is not None:
        interest_groups_base_query = interest_groups_base_query\
            .filter(InterestGroups.total >= total_contributions_start)\
            .filter(InterestGroups.total < total_contributions_end)
    if dem_contributions_start is not None and dem_contributions_end is not None:
        interest_groups_base_query = interest_groups_base_query.filter(InterestGroups.dems >= dem_contributions_start)\
            .filter(InterestGroups.dems < dem_contributions_end)
    if repub_contributions_start is not None and repub_contributions_end is not None:
        interest_groups_base_query = interest_groups_base_query\
            .filter(InterestGroups.repubs >= repub_contributions_start)\
            .filter(InterestGroups.repubs < repub_contributions_end)
    if pac_contributions_start is not None and pac_contributions_end is not None:
        interest_groups_base_query = interest_groups_base_query\
            .filter(InterestGroups.gave_to_pac >= pac_contributions_start)\
            .filter(InterestGroups.gave_to_pac < pac_contributions_end)

    # Searching
    if search is not None:
        words = search.split()
        conditions = []
        for word in words:
            conditions.append(
                func.concat(
                    InterestGroups.name,
                    InterestGroups.total,
                    InterestGroups.dems,
                    InterestGroups.repubs,
                    InterestGroups.gave_to_pac,
                ).like("%" + word + "%")
            )
        interest_groups_base_query = interest_groups_base_query.filter(or_(*conditions))

    # Sorting
    if sort_column is not None and asc_or_desc is not None:
        if sort_column == "name":
            if asc_or_desc == "asc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.name.asc())
            elif asc_or_desc == "desc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.name.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "total_contributions":
            if asc_or_desc == "asc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.total.asc())
            elif asc_or_desc == "desc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.total.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "dem_contributions":
            if asc_or_desc == "asc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.dems.asc())
            elif asc_or_desc == "desc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.dems.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "repub_contributions":
            if asc_or_desc == "asc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.repubs.asc())
            elif asc_or_desc == "desc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.repubs.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "pac_contributions":
            if asc_or_desc == "asc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.gave_to_pac.asc())
            elif asc_or_desc == "desc":
                interest_groups_base_query = interest_groups_base_query.order_by(InterestGroups.gave_to_pac.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        else:
            return "Unknown sort_column, " + sort_column, 400

    response = dict()
    response["size"] = interest_groups_base_query.count()
    interest_groups = interest_groups_base_query.limit(10).offset(10 * offset - 10)
    response["data"] = [ig.as_dict() for ig in interest_groups]
    return jsonify(response), 200


@interest_groups_api.route("/getAllInterestGroups", methods=["GET"])
@cross_origin()
def get_all_interest_groups():
    interest_groups = InterestGroups.query.all()
    response = [ig.as_dict() for ig in interest_groups]
    return jsonify(response), 200
