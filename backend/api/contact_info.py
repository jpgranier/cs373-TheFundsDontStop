from backend.models.models import ContactInfo
from flask_cors import cross_origin
from flask import request
from flask import jsonify
from flask import Blueprint
import json
from sqlalchemy import func
from sqlalchemy import or_

contact_info_api = Blueprint("contact_info", __name__, template_folder="templates")


@contact_info_api.route("/getContactInfoByVoteSmartId", methods=["GET"])
@cross_origin()
def get_contact_info():
    contact_info = ContactInfo.query.filter_by(
        votesmart_id=request.args.get("votesmart_id")
    ).first()
    return json.dumps(contact_info.as_dict()), 200


@contact_info_api.route("/getCandidateIds")
@cross_origin()
def get_candidate_ids():
    contact_info_list = ContactInfo.query.all()
    id_list = []
    for contact_info in contact_info_list:
        id_list.append(
            {
                "id": contact_info.id,
                "candidate_votesmart_id": contact_info.votesmart_id,
                "candidate_name": contact_info.first_name
                + " "
                + contact_info.last_name,
            }
        )
    response = jsonify(id_list)
    return response, 200


@contact_info_api.route("/getCandidateInfo", methods=["GET"])
@cross_origin()
def get_candidate_info():
    candidate = ContactInfo.query.filter_by(id=request.args.get("id")).first()
    response = json.dumps(candidate.as_dict())
    return response, 200


@contact_info_api.route("/getAllCandidateInfo", methods=["GET"])
@cross_origin()
def get_all_candidate_info():
    candidate_list = ContactInfo.query.all()
    response = jsonify([candidate.as_dict() for candidate in candidate_list])
    return response, 200


@contact_info_api.route("/getCandidateInfoRangeFilterSearchAndSort", methods=["GET"])
@cross_origin()
def get_candidate_info_range_filter_search_and_sort():
    offset = int(request.args.get("offset"))
    if offset < 1:
        return "", 400

    first_name_first_letter = request.args.get("first_name_first_letter")
    last_name_first_letter = request.args.get("last_name_first_letter")
    party = request.args.get("party")
    state = request.args.get("state")
    office = request.args.get("office")

    search = request.args.get("search")

    sort_column = request.args.get("sort_column")
    asc_or_desc = request.args.get("asc_or_desc")

    contact_info_base_query = ContactInfo.query

    # Filtering
    if first_name_first_letter is not None:
        contact_info_base_query = contact_info_base_query\
            .filter(ContactInfo.first_name.startswith(first_name_first_letter))
    if last_name_first_letter is not None:
        contact_info_base_query = contact_info_base_query.\
            filter(ContactInfo.last_name.startswith(last_name_first_letter))
    if party is not None:
        contact_info_base_query = contact_info_base_query.filter_by(party=party)
    if state is not None:
        contact_info_base_query = contact_info_base_query.filter_by(state=state)
    if office is not None:
        contact_info_base_query = contact_info_base_query.filter_by(office=office)

    # Searching
    if search is not None:
        words = search.split()
        conditions = []
        for word in words:
            conditions.append(
                func.concat(
                    ContactInfo.first_name,
                    ContactInfo.last_name,
                    ContactInfo.party,
                    ContactInfo.state,
                    ContactInfo.office,
                ).like("%" + word + "%")
            )
        contact_info_base_query = contact_info_base_query.filter(or_(*conditions))

    # Sorting
    if sort_column is not None and asc_or_desc is not None:
        if sort_column == "first_name":
            if asc_or_desc == "asc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.first_name.asc())
            elif asc_or_desc == "desc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.first_name.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "last_name":
            if asc_or_desc == "asc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.last_name.asc())
            elif asc_or_desc == "desc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.last_name.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "party":
            if asc_or_desc == "asc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.party.asc())
            elif asc_or_desc == "desc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.party.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "state":
            if asc_or_desc == "asc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.state.asc())
            elif asc_or_desc == "desc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.state.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        elif sort_column == "office":
            if asc_or_desc == "asc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.office.asc())
            elif asc_or_desc == "desc":
                contact_info_base_query = contact_info_base_query.order_by(ContactInfo.office.desc())
            else:
                return "Unknown asc_or_desc, " + asc_or_desc, 400
        else:
            return "Unknown sort_column, " + sort_column, 400

    response = dict()
    response["size"] = contact_info_base_query.count()
    contact_info = contact_info_base_query.limit(10).offset(10 * offset - 10)
    response["data"] = [ci.as_dict() for ci in contact_info]
    return jsonify(response), 200


@contact_info_api.route("/getAllCandidateInfoParties", methods=["GET"])
@cross_origin()
def get_all_candidate_info_parties():
    parties = []
    for contact_info in (
        ContactInfo.query.with_entities(ContactInfo.party)
        .order_by(ContactInfo.party.asc())
        .distinct()
    ):
        parties.append(contact_info.party)
    response = jsonify(parties)
    return response, 200


@contact_info_api.route("/getAllCandidateInfoStates", methods=["GET"])
@cross_origin()
def get_all_candidate_info_states():
    states = []
    for contact_info in (
        ContactInfo.query.with_entities(ContactInfo.state)
        .order_by(ContactInfo.state.asc())
        .distinct()
    ):
        states.append(contact_info.state)
    response = jsonify(states)
    return response, 200


@contact_info_api.route("/getAllCandidateInfoOffices", methods=["GET"])
@cross_origin()
def get_all_candidate_info_offices():
    offices = []
    for contact_info in (
        ContactInfo.query.with_entities(ContactInfo.office)
        .order_by(ContactInfo.office.asc())
        .distinct()
    ):
        offices.append(contact_info.office)
    response = jsonify(offices)
    return response, 200
