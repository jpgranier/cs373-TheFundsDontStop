from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class CandidateBills(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bill_id = db.Column(db.String(6))  # Votesmart Bill ID
    bill_number = db.Column(db.String(13))  # equivalent to what Propublica looks for
    title = db.Column(db.String(370))
    officeId = db.Column(db.String(2))
    office = db.Column(db.String(15))
    actionId = db.Column(db.String(6))
    stage = db.Column(db.String(37))
    vote = db.Column(db.String(2))
    categories = db.Column(db.String(20))
    candidateId = db.Column(db.String(7))

    def __repr__(self):
        return "<Bill ID: {}>".format(self.bill_id)

    def as_dict(self):
        return {cb.name: getattr(self, cb.name) for cb in self.__table__.columns}


class VotesmartBills(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bill_id = db.Column(db.String(6))
    bill_number = db.Column(db.String(14))
    title = db.Column(db.String(370))
    date_introduced = db.Column(db.String(5))
    bill_type = db.Column(db.String(34))
    sponsors = db.Column(db.String(1000))
    categories = db.Column(db.String(4))
    committee_sponsors = db.Column(db.String(97))
    # there's a lot of info in an action, i'll just store actionId and make a separate table for the specifics of
    # actions if there is time
    actionIds = db.Column(db.String(60))
    primary_sponsor = db.Column(db.String(256))

    def __repr__(self):
        return "<Bill ID: {}>".format(self.bill_id)

    def as_dict(self):
        return {vb.name: getattr(self, vb.name) for vb in self.__table__.columns}


class ContactInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(12))
    last_name = db.Column(db.String(18))
    party = db.Column(db.String(3))
    district = db.Column(db.String(20))
    twitter_account = db.Column(db.String(16))
    facebook_account = db.Column(db.String(30))
    youtube_account = db.Column(db.String(21))
    votesmart_id = db.Column(db.String(7))
    crp_id = db.Column(db.String(10))
    website_url = db.Column(db.String(40))
    image_url = db.Column(db.String(49))
    state = db.Column(db.String(3))
    office = db.Column(db.String(6))

    def __repr__(self):
        return "<Contact Info for {} {}>".format(self.first_name, self.last_name)

    def as_dict(self):
        return {ci.name: getattr(self, ci.name) for ci in self.__table__.columns}


class Ratings(db.Model):
    rating_id = db.Column(db.Integer, primary_key=True)
    candidate_votesmart_id = db.Column(db.String(7))
    sig_id = db.Column(db.Integer)
    sig_votesmart_id = db.Column(db.Integer)
    rating = db.Column(db.String(8))
    timespan = db.Column(db.String(10))
    rating_name = db.Column(db.String(73))

    def as_dict(self):
        return {ig.name: getattr(self, ig.name) for ig in self.__table__.columns}


class InterestGroups(db.Model):
    sig_id = db.Column(db.Integer, primary_key=True)
    votesmart_id = db.Column(db.String(5))
    category_id = db.Column(db.Integer)
    name = db.Column(db.String(118))
    description = db.Column(db.String(2164))
    address = db.Column(db.String(68))
    phone = db.Column(db.String(13))
    dems = db.Column(db.Integer)
    repubs = db.Column(db.Integer)
    gave_to_pac = db.Column(db.Integer)
    gave_to_cand = db.Column(db.Integer)
    total = db.Column(db.Integer)
    logo_url = db.Column(db.String(200))
    
    def as_dict(self):
        return {ig.name: getattr(self, ig.name) for ig in self.__table__.columns}


class CandidateFinancialContributions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(12))
    last_name = db.Column(db.String(18))
    crp_id = db.Column(db.String(10))
    ind1 = db.Column(db.String(49))
    ind2 = db.Column(db.String(49))
    ind3 = db.Column(db.String(49))
    ind4 = db.Column(db.String(49))
    ind5 = db.Column(db.String(48))

    def __repr__(self):
        return "<Recent Candidate: {}>".format(self.first_name, self.last_name)

    def as_dict(self):
        return {cfc.name: getattr(self, cfc.name) for cfc in self.__table__.columns}
