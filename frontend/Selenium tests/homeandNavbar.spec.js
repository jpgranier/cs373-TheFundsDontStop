// Generated by Selenium IDE
const { Builder, By, Key, until } = require('selenium-webdriver')
const assert = require('assert')

describe('Home and Navbar', function() {
  this.timeout(30000)
  let driver
  let vars
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build()
    vars = {}
  })
  afterEach(async function() {
    await driver.quit();
  })
  it('Home and Navbar', async function() {
    await driver.get("https://thefundsdontstop.me/")
    await driver.manage().window().setRect(1376, 744)
    await driver.findElement(By.linkText("Home")).click()
    await driver.findElement(By.css(".col-sm-4:nth-child(4) .btn")).click()
    await driver.executeScript("window.scrollTo(0,0)")
    await driver.findElement(By.linkText("Home")).click()
    await driver.findElement(By.css(".col-sm-4:nth-child(5) .btn")).click()
    await driver.executeScript("window.scrollTo(0,0)")
    await driver.findElement(By.linkText("Home")).click()
    await driver.findElement(By.css(".col-sm-4:nth-child(6) .btn")).click()
    await driver.executeScript("window.scrollTo(0,0)")
    await driver.findElement(By.linkText("Home")).click()
    await driver.findElement(By.linkText("Interest Groups")).click()
    await driver.findElement(By.linkText("Candidates")).click()
    await driver.findElement(By.linkText("Legislation")).click()
    await driver.findElement(By.linkText("About")).click()
    await driver.findElement(By.linkText("Home")).click()
  })
})
