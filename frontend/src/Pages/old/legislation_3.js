import React, { Component } from 'react';

export const legislation3 = () => {
    return (
        <html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>

    <title>The Funds Don't Stop</title>
  </head>
  <body>
    <div class="container">
      <h1 class="my-4">Education</h1>

      <div class="row">
        <div class="col-md-8">
          <img class="img-fluid" src="https://graphiccave.com/wp-content/uploads/2015/06/Knowledge-and-Education-Symbols-Vectors-JPG-Graphic-Cave-1080x565.jpg" alt=""/>
        </div>

        <div class="col-md-4">
          <h3 class="my-3">A Few Relevant Bills</h3>
          <ul>
            <li> <a href="https://www.govtrack.us/congress/bills/116/s2779">S. 2779: Luke and Alex School Safety Act of 2019</a></li>
            <li> <a href="https://www.govtrack.us/congress/bills/116/hr2528">H.R. 2528: STEM Opportunities Act of 2019</a></li>
            <li> <a href="https://www.govtrack.us/congress/bills/116/hr2486">H.R. 2486: FUTURE Act</a></li>
          </ul>
          <h3 class="my-3">Candidates in Favor</h3>
          <ul>
             <li>Bernie Sanders</li> 
             <li>Elizabeth Warren</li>
             <li>Pete Buttigieg</li>
          </ul>
          <h3 class="my-3">Recent News</h3>
          <ul>
             <li><a href="https://www.wtap.com/content/news/Bible-education-bill-advances-in-WVa-Legislature-568212691.html">Bible education bill advances in W.Va. Legislature </a></li> 
          </ul>
          
        </div>
      </div>

      <h3 class="my-4">Related Interest Groups</h3>

      <div class="row">
        <div class="col-md-3 col-sm-6 mb-4">
          <a href="http://www.nea.org"> 
            <img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRGyl6lWGZrUSgAKzYf6t-OvX6FFq8s0P6qcEsrzpVuKtnfn8Dc" alt=""/>
          </a>
        </div>
      </div>

    </div>  
  </body>
</html>
    )}