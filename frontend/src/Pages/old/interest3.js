import React, { Component } from 'react';

export const interest3 = () => {
    return(
        <div class="container">
  <h1 class="my-4">Planned Parenthood Action Fund
  </h1>
  <div class="row">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
    <div class="col-md-16">
      <img class="img-fluid" src="https://www.issueone.org/wp-content/uploads/2018/09/PPAF-logo.png" alt=""/>
    </div>
  </div>
    <div class="col-md-16">
      <h3 class="my-3">Description</h3>
      <p>"The Planned Parenthood® Action Fund, the host of www.plannedparenthoodaction.org, is a national not-for-profit organization with its main offices in New York City and Washington, DC. The Action Fund is the nonpartisan advocacy and political arm of Planned Parenthood Federation of America. The Action Fund engages in educational and electoral activity, including legislative advocacy, voter education, and grassroots organizing to promote the Planned Parenthood mission. Planned Parenthood Federation of America, Inc. (PPFA) is the nation's leading provider and advocate of high-quality, affordable health care for women, men, and young people, as well as the nation's largest provider of sex education. With over 650 health centers across the country, Planned Parenthood organizations serve all patients with care and compassion, with respect and without judgment. Through health centers, programs in schools and communities, and online resources, Planned Parenthood is a trusted source of reliable health information that allows people to make informed health decisions."</p>
    </div>
    <h3 class="my-3">Categories</h3>
      <ul class="list-group">
        <li class="list-group-item">Abortion</li>
        <li class="list-group-item">Abortion and Reproductive</li>
        <li class="list-group-item">Health and Healthcare</li>
      </ul>
    <div class="col-md-16">
      <h3 class="my-3">Ratings</h3>
       <table class="table">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Office</th>
        <th scope="col">State</th>
        <th scope="col">Party</th>
        <th scope="col">Rating</th>
        <th scope="col">Year of Rating</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">Amy Klobuchar</th>
        <td>U.S. Senate</td>
        <td> MN </td>
        <td>Democratic</td>
        <td>100%</td>
        <td>2019</td>
      </tr>
      <tr>
        <th scope="row"> <a href="Elizabeth Warren">Elizabeth Warren</a></th>
        <td>U.S Senate</td>
        <td>MA</td>
        <td>Democratic</td>
        <td>100%</td>
        <td>2019</td>
      </tr>
      <tr>
        <th scope="row"> <a href="Bernie Sanders">Bernie Sanders</a></th> 
        <td>U.S Senate</td>
        <td>VT</td>
        <td>Independent</td>
        <td>100%</td>
        <td>2018</td>
      </tr>
      <tr>
        <th scope="row">John Cornyn</th>
        <td>U.S Senate</td>
        <td>TX</td>
        <td>Republican</td>
        <td>0%</td>
        <td>2019</td>
      </tr>
    </tbody>
  </table>


  <h3 class="my-4">2020 Endorsements</h3>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Current Office</th>
        <th scope="col">Running</th>
        <th scope="col">State</th>
        <th scope="col">Party</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">Joaquin Castro</th>
        <td>U.S. House</td>
        <td>U.S House </td>
        <td>TX</td>
        <td>Democratic</td>
      </tr>
      <tr>
        <th scope="row">Wendy Davis</th>
        <td>N/A</td>
        <td>U.S House</td>
        <td>TX</td>
        <td>Democratic</td>
      </tr>
      <tr>
        <th scope="row">Lizzie Fletcher</th>
        <td>U.S House</td>
        <td>U.S. House</td>
        <td>TX</td>
        <td>Deomcratic</td>
      </tr>
      <tr>
        <th scope="row">Jessica Cisneros</th>
        <td>N/A</td>
        <td>U.S House</td>
        <td>TX</td>
        <td>Democratic</td>
        
      </tr>
    </tbody>
  </table>
    </div>
  <div class="row">
  

  </div>
 <h3 class="my-4">2020 Top Candidate Contributions</h3>
 <table class="table">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Office</th>
        <th scope="col">Party</th>
        <th scope="col">Amount</th>
      </tr>

    </thead>
    <tbody>
      <tr>
        <th scope="row">Nancy Pelosi</th>
        <td>U.S. House</td>
        <td>Democratic</td>
        <td>$5,000</td>
      </tr>
      <tr>
        <th scope="row">James Clyburn</th>
        <td>U.S. House</td>
        <td>Democratic</td>
        <td>$5,000</td>
      </tr>
      <tr>
        <th scope="row">Marie Newman</th>
        <td>U.S. House</td>
        <td>Democratic</td>
        <td>$3,199</td>
      </tr>
      <tr>
        <th scope="row">Mark Kelly</th>
        <td>U.S. Senate</td>
        <td>Democratic</td>
        <td>$2,500</td>
      </tr>
      <tr>
        <th scope="row">Ben Ray Lujan</th>
        <td>U.S. Senate</td>
        <td>Democratic</td>
        <td>$2,500</td>
      </tr>
        </tbody>
        </table>
    </div>    
    )
}