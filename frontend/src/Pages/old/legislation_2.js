import React, { Component } from 'react';

export const legislation2 = () => {
    return (<html lang="en">
    <head>
      <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
  
      <title>The Funds Don't Stop</title>
    </head>
    <body>
      <div class="container">
        <h1 class="my-4">Firearms and Explosives</h1>
  
        <div class="row">
          <div class="col-md-5">
            <img class="img-fluid" src="https://i.ya-webdesign.com/images/transparent-guns-cartoon-2.png" alt=""/>
          </div>
  
          <div class="col-md-4">
            <h3 class="my-3">A Few Relevant Bills</h3>
            <ul>
              <li> <a href="https://www.govtrack.us/congress/bills/116/hr2745">H.R. 2745: Military Construction, Veterans Affairs, and Related Agencies Appropriations Act, 2020</a> </li>
              <li> <a href="https://www.govtrack.us/congress/bills/116/hr1437">H.R. 1437: Securing Department of Homeland Security Firearms Act of 2019</a> </li>
              <li> <a href="https://www.govtrack.us/congress/bills/116/s47"> S. 47: John D. Dingell, Jr. Conservation, Management, and Recreation Act</a> </li>
            </ul>
            <h3 class="my-3">Candidates in Favor</h3>
            <ul>
               <li>Bernie Sanders</li>
               <li>Elizabeth Warren</li>
               <li>Pete Buttigieg</li>
            </ul>
            <h3 class="my-3">Recent News</h3>
            <ul>
               <li> <a href="https://www.washingtonexaminer.com/politics/virginia-senate-committee-advances-slew-of-conformed-gun-control-bills">Virginia Senate committee advances slew of conformed gun-control bills </a></li> 
            </ul>
            
          </div>
        </div>
  
        <h3 class="my-4">Related Interest Groups</h3>
  
        <div class="row">
          <div class="col-md-3 col-sm-6 mb-4">
            <a href="https://home.nra.org">
              <img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/en/thumb/c/c7/National_Rifle_Association_official_logo.svg/1200px-National_Rifle_Association_official_logo.svg.png" alt=""/>
            </a>
          </div>
        </div>
  
      </div>  
    </body>
  </html>)}