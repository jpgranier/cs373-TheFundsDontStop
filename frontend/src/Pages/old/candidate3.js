import React from 'react';
import { Fragment } from 'react';
import pb from '../pb.jpg'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

export class candidate3 extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <Fragment>
      <h1> Pete Buttgieg : Democrat</h1>
      <img src = {pb} alt="pb" />
      <p> District: D-IN </p>
      <p> Top contributor: Alphabet Inc </p>
      <a href= {"https://justfacts.votesmart.org/candidate/campaign-finance/127151/pete-buttigieg"}> Source </a>
      <p> Important issues: </p>
      <p> Polls </p>
      <a href= {"https://www.realclearpolitics.com/epolls/2020/president/us/2020_democratic_presidential_nomination-6730.html"}> Click here </a>
      <p> Positions </p>
      <a href= {"https://justfacts.votesmart.org/candidate/political-courage-test/127151/pete-buttigieg"}> Click here </a>
      <p> Elections participated: 2020 Presidential </p>      
      </Fragment>
  }
}