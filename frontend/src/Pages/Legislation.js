import React, { Component } from 'react';
import {Container, Row, Col, Image, Button, Table, Card, Dropdown, DropdownButton} from 'react-bootstrap';
import Highlighter from "react-highlight-words";
import styles from './highlighter.css'
import ModelPagination from "../Components/ModelPagination"
const url = "https://api.thefundsdontstop.me/getVotesmartBillRangeFilterSearchAndSort?offset="
const filter = false
const perPage = 10

export default class Legislation extends Component {
   constructor(props) {
      super(props) 
      this.state = { 
         loaded: false,
         Bills: [],
         offset: 1,
         bill_number_prefix: "",
         title_first_letter: "",
         date_introduced: "",
         primary_sponsor_first_letter: "",
         bill_type: "",
         search: "",
         sort_column: "",
         asc_or_desc: "",
         size: -1,
         parameters: "",
         url: "",
         formVal: "",
      }

      this.state.parameters = this.state.bill_number_prefix+this.state.title_first_letter+this.state.date_introduced+this.state.primary_sponsor_first_letter+this.state.search+this.state.sort_column+this.state.asc_or_desc+this.state.bill_type
      this.state.url = url+this.state.offset+this.state.parameters

      this.changePage = this.changePage.bind(this);
      this.updateParameters = this.updateParameters.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.firstPage = this.firstPage.bind(this);
      this.previousPage = this.previousPage.bind(this);
      this.nextPage = this.nextPage.bind(this);
      this.lastPage = this.lastPage.bind(this);
      this.selectPage = this.selectPage.bind(this);
      this.prevEll = this.prevEll.bind(this);
      this.nextEll = this.nextEll.bind(this);
      this.resetFilters = this.resetFilters.bind(this);
   }

   renderTableData() {
    return this.state.Bills.map((Bill) => {
       return (

            <Card style={{width:"20%", height:"20%"}}>
               <Card.Body>
                  <Card.Title> 
                     <a href={'bills/'+ Bill.bill_id}>
                        {<Highlighter
                           activeClassName={styles.Active}
                           highlightClassName={styles.Highlight}
                           searchWords={[this.state.formVal]}
                           textToHighlight={Bill.bill_number}
                           highlightStyle={{ fontWeight: 'bold' }}/>}
                     </a>
                  </Card.Title>
                  <Card.Text>
                     <p>
                     {<Highlighter
                        activeClassName={styles.Active}
                        highlightClassName={styles.Highlight}
                        searchWords={[this.state.formVal]}
                        textToHighlight={Bill.title}
                        highlightStyle={{ fontWeight: 'bold' }}/>}
                        <br></br>
                        <br></br>
            
                     Type: {<Highlighter
                              activeClassName={styles.Active}
                              highlightClassName={styles.Highlight}
                     searchWords={[this.state.formVal]}
                     textToHighlight={Bill.bill_type}
                     highlightStyle={{ fontWeight: 'bold' }}/>}
                     
                     <br></br>
                     
                     Introduced: {<Highlighter
                              activeClassName={styles.Active}
                              highlightClassName={styles.Highlight}
                     searchWords={[this.state.formVal]}
                     textToHighlight={Bill.date_introduced}
                     highlightStyle={{ fontWeight: 'bold' }}/>}
                     
                     <br></br>
                     <br></br>
                     
                     Primary Sponsor: {<Highlighter
                              activeClassName={styles.Active}
                              highlightClassName={styles.Highlight}
                     searchWords={[this.state.formVal]}
                     textToHighlight={Bill.primary_sponsor}
                     highlightStyle={{ fontWeight: 'bold' }}/>}
                     
                     <br></br>

                     </p>
                  </Card.Text>
               </Card.Body>
            </Card>

       )
    })
 }


 async changePage(offset) {
   this.setState({loaded: false, offset : offset})
   this.state.url = url+offset+this.state.parameters
   const response = await fetch(this.state.url)
   const data = await response.json();
   this.setState({loaded: true, Bills: data.data, size: Math.ceil(data.size/perPage)})
}

async updateParameters() {
   const offset = 1
   this.state.parameters = this.state.bill_number_prefix+this.state.title_first_letter+this.state.date_introduced+this.state.primary_sponsor_first_letter+this.state.search+this.state.sort_column+this.state.asc_or_desc+this.state.bill_type
   this.state.url = url+offset+this.state.parameters

   this.setState({loaded : false})
      const response = await fetch(this.state.url)
      const data = await response.json();
      this.setState({loaded: true, Bills: data.data, size: Math.ceil(data.size/perPage), offset: 1})

}


//---pagination---

async firstPage() {
   const offset2 = 1
   this.changePage(offset2)
}

async prevEll(){
   const offset2 = Math.max(this.state.offset - 3, 1)
   this.changePage(offset2)
}

async previousPage() {
   const offset2 = this.state.offset - 1
   this.changePage(offset2)
}

async nextPage() {
   const offset2 = this.state.offset + 1
   this.changePage(offset2)
}

async nextEll(){
   const offset2 = Math.min(this.state.size, this.state.offset + 3)
   this.changePage(offset2)
}

async lastPage() {
   const offset2 = this.state.size
   this.changePage(offset2)
}

async selectPage(index){
   const offset2 = index
   this.changePage(offset2)
}


async billNumberSortAsce() {
   if (this.state.sort_column == "&sort_column=bill_number" && this.state.asc_or_desc == "&asc_or_desc=asc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=bill_number" 
      this.state.asc_or_desc = "&asc_or_desc=asc"
      this.state.offset = 1
   }
   this.updateParameters()     
}

async billNumberSortDesc() {
   if (this.state.sort_column == "&sort_column=bill_number" && this.state.asc_or_desc == "&asc_or_desc=desc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=bill_number" 
      this.state.asc_or_desc = "&asc_or_desc=desc"
      this.state.offset = 1
   }
   this.updateParameters()    

}

async billTitleSortAsce() {
   if (this.state.sort_column == "&sort_column=title" && this.state.asc_or_desc == "&asc_or_desc=asc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=title" 
      this.state.asc_or_desc = "&asc_or_desc=asc"
      this.state.offset = 1
   }
   this.updateParameters()    

}

async billTitleSortDesc() {
   if (this.state.sort_column == "&sort_column=title" && this.state.asc_or_desc == "&asc_or_desc=desc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=title" 
      this.state.asc_or_desc = "&asc_or_desc=desc"
      this.state.offset = 1
   }
   this.updateParameters()   

}

async billDateSortAsce() {
   if (this.state.sort_column == "&sort_column=date_introduced" && this.state.asc_or_desc == "&asc_or_desc=asc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=date_introduced" 
      this.state.asc_or_desc = "&asc_or_desc=asc"
      this.state.offset = 1
   }
   this.updateParameters()   

}

async billDateSortDesc() {
   if (this.state.sort_column == "&sort_column=date_introduced" && this.state.asc_or_desc == "&asc_or_desc=desc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=date_introduced" 
      this.state.asc_or_desc = "&asc_or_desc=desc"
      this.state.offset = 1
   }
   this.updateParameters() 

}

async billTypeSortAsce() {
   if (this.state.sort_column == "&sort_column=bill_type" && this.state.asc_or_desc == "&asc_or_desc=asc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=bill_type" 
      this.state.asc_or_desc = "&asc_or_desc=asc"
      this.state.offset = 1
   }
   this.updateParameters() 

}

async billTypeSortDesc() {
   if (this.state.sort_column == "&sort_column=bill_type" && this.state.asc_or_desc == "&asc_or_desc=desc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=bill_type" 
      this.state.asc_or_desc = "&asc_or_desc=desc"
      this.state.offset = 1
   }
   this.updateParameters() 

}

async billSponsorSortAsce() {
   if (this.state.sort_column == "&sort_column=primary_sponsor" && this.state.asc_or_desc == "&asc_or_desc=asc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=primary_sponsor" 
      this.state.asc_or_desc = "&asc_or_desc=asc"
      this.state.offset = 1
   }
   this.updateParameters() 

}

async billSponsorSortDesc() {
   if (this.state.sort_column == "&sort_column=primary_sponsor" && this.state.asc_or_desc == "&asc_or_desc=desc") {
      this.state.sort_column = ""
      this.state.asc_or_desc = ""
   }
   else {
      this.state.sort_column = "&sort_column=primary_sponsor" 
      this.state.asc_or_desc = "&asc_or_desc=desc"
      this.state.offset = 1
   }
   this.updateParameters() 

}


async houseFilter() {
   if (this.state.bill_number_prefix == "&bill_number_prefix=H") {
      this.state.bill_number_prefix = ""
   }
   else {
      this.state.bill_number_prefix = "&bill_number_prefix=H"
      this.state.offset = 1
   }
   this.updateParameters()

}

async senateFilter() {
   if (this.state.bill_number_prefix == "&bill_number_prefix=S") {
      this.state.bill_number_prefix = ""
   }
   else {
      this.state.bill_number_prefix = "&bill_number_prefix=S"
      this.state.offset = 1
   }
   this.updateParameters()

}

async presidentFilter() {
   if (this.state.bill_number_prefix == "&bill_number_prefix=P") {
      this.state.bill_number_prefix = ""
   }
   else {
      this.state.bill_number_prefix = "&bill_number_prefix=P"
      this.state.offset = 1
   }
   this.updateParameters()

}

async billTitleFilter(letter) {
   if (this.state.title_first_letter == "&title_first_letter=" + letter) {
      this.state.title_first_letter = ""
   }
   else {
      this.state.title_first_letter = "&title_first_letter=" + letter
      this.state.offset = 1
   }
   this.updateParameters()

}

async billTypeFilter(type) {
   if (this.state.bill_type == "&bill_type=" + type) {
      this.state.bill_type = ""
   }
   else {
      this.state.bill_type = "&bill_type=" + type
      this.state.offset = 1
   }
   this.updateParameters()

}

async billDateFilter(year) {
   if (this.state.date_introduced == "&date_introduced=" + year) {
      this.state.date_introduced = ""
   }
   else {
      this.state.date_introduced = "&date_introduced=" + year
      this.state.offset = 1
   }
   this.updateParameters()

}

async billSponsorFilter(letter) {
   if (this.state.primary_sponsor_first_letter == "&primary_sponsor_first_letter=" + letter) {
      this.state.primary_sponsor_first_letter = ""
   }
   else {
      this.state.primary_sponsor_first_letter = "&primary_sponsor_first_letter=" + letter
      this.state.offset = 1
   }
   this.updateParameters()

}

async componentDidMount() {
   const response = await fetch(this.state.url);
   const data = await response.json();
   this.setState({loaded: true, Bills: data.data, size: Math.ceil(data.size/perPage)});
  
   
}


renderSortingButtonsUpDown(asce_f, desc_f, t){
   return(
      <span style={{fontSize: "20px"}}>
            <button type="button" class="btn btn-success btn-sm" onClick={()=> {asce_f.call(t)}}>
               {'↑'}      
            </button>
            <button type="button" class="btn btn-danger btn-sm" onClick={()=> {desc_f.call(t)}}>
               {'↓'}
            </button>
         </span>
   )
}

renderSortingButtonsCategory(asce_f, desc_f, text1, text2, t){
   return(
      <span>
         <button type="button" class="btn btn-primary btn-sm" onClick={()=> {asce_f.call(t)}}>
            {text1}
         </button>
         <button type="button" class="btn btn-danger btn-sm" onClick={()=> {desc_f.call(t)}}>
            {text2}
         </button>
      </span>
   )
}


renderSingleFuncDropdown(title, items, func, t){
   return(
      <DropdownButton variant='info' size='lg' id="dropdown-basic-button" title={title} drop="down">
         <div style={{maxHeight: '300px', overflow: 'scroll'}}>
            {items.map((item_title, index) => {
               return(
                  <Dropdown.Item onClick={() => {func.call(t, item_title)}}>
                     {item_title}
                  </Dropdown.Item>
               );
            
            })}
         </div>
      </DropdownButton>
   )
}


renderDropdown(title, items, funcs, t){
   return(
      <DropdownButton variant='info' size='lg' id="dropdown-basic-button" title={title} drop="down">
            {items.map((item_title, index) => {
               return(
                  <Dropdown.Item onClick={() => {funcs[index].call(t)}}>
                     {item_title}
                  </Dropdown.Item>
               );
            
            })}
      </DropdownButton>
   )
}


 renderButtons() {


   let rows = []

      rows.push(
         <ModelPagination  
         size={this.state.size}
         current={this.state.offset}
         selectPage={this.selectPage}
         prev={this.previousPage}
         next={this.nextPage}
         first={this.firstPage}
         last={this.lastPage}
         nextEll={this.nextEll}
         prevEll={this.prevEll}
      />

      )
   const titleLetters = ['A','B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'Z']
   const sponsorLetters = ['A','B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Z']
   const years = ["2020","2019", "2018", "2017", "2016","2015","2014"]
   const types = ["Amendment", "Legislation", "Legislation-Joint Resolution", "Legislation-Resolution", "Nomination"]

   rows.push(
      <React.Fragment>
         <tr style={{fontSize: '22px'}}>
         <th scope="col" class="text-center">
               {this.renderDropdown("Bill Chamber", ["House", "Senate", "Executive"], [this.houseFilter, this.senateFilter, this.presidentFilter], this)} 
               <br/>
               {this.renderSortingButtonsUpDown(this.billNumberSortAsce, this.billNumberSortDesc, this)}
            </th>
            <th scope="col" class="text-center" >
               {this.renderSingleFuncDropdown("Title", titleLetters, this.billTitleFilter, this)}
               <br/>
               {this.renderSortingButtonsCategory(this.billTitleSortAsce, this.billTitleSortDesc, 'A-Z', 'Z-A', this)}
            </th>
            <th scope="col" class="text-center">
               {this.renderSingleFuncDropdown("Date Introduced", years, this.billDateFilter, this)} 
               <br/>
               {this.renderSortingButtonsUpDown(this.billDateSortAsce, this.billDateSortDesc, this)}
            </th>
            <th scope="col" class="text-center">
               {this.renderSingleFuncDropdown("Type", types, this.billTypeFilter, this)}
               <br/>
               {this.renderSortingButtonsUpDown(this. billTypeSortAsce, this. billTypeSortDesc, this)}
            </th>
            <th scope="col" class="text-center">
               {this.renderSingleFuncDropdown("Primary Sponsor", sponsorLetters, this.billSponsorFilter, this)}
               <br/>
               {this.renderSortingButtonsCategory(this. billSponsorSortAsce, this. billSponsorSortDesc, 'A-Z', 'Z-A', this)}
            </th>
         </tr>

      </React.Fragment>
   )

   return(
      <thead>
         {rows}
         
      </thead>
   )
}

handleChange(event){
   this.state.formVal = event.target.value
   if (this.state.formVal == "") {
      this.state.search = ""
      this.updateParameters()
   }
   else {
      this.search()
   }
  
   this.setState({loaded: true})
}

handleSubmit(event) {
   if (this.state.formVal == "") {
      this.state.search = ""
      this.updateParameters()
   }
   else {
      this.setState({loaded:false})
      this.search()
      this.setState({loaded:true})
   }

   event.preventDefault();
}

resetFilters() {
   let newstate = { 
      loaded: false,
      Bills: [],
      offset: 1,
      bill_number_prefix: "",
      title_first_letter: "",
      date_introduced: "",
      primary_sponsor_first_letter: "",
      bill_type: "",
      search: "",
      sort_column: "",
      asc_or_desc: "",
      size: -1,
      parameters: "",
      url: "",
      formVal: "",
   }
   this.state = newstate
   this.setState(newstate)
   this.updateParameters()
   //this.search()
   //this.render()
}

async search() {
   this.state.search = "&search=" + this.state.formVal.replace(/\s+/g, '')
   this.updateParameters()
}


  
   render() { 
      
      if (!this.state.loaded) {
         return (<div>Loading...</div>)
      }
      return (
         <div>
            
            <button type="button" class="btn btn-primary btn-sm" onClick={()=> {this.resetFilters.call()}}>
               {'Reset Filters'}      
            </button>
            <h1 id = 'title'>Legislation</h1>
            <h4>Page: {this.state.offset}/{this.state.size}</h4>
            <form onSubmit={this.handleSubmit}>
               <label>
                  <input type="text" value={this.state.formVal} onChange={this.handleChange}/>
               </label>
               <input type="submit" value="Search"/>
            </form>
            <table class = 'table'>
               {this.renderButtons()}
            </table>
            <Row>
               {this.renderTableData()}
            </Row>
         </div>
     )
   }
}