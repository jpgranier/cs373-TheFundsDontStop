import React, { Component } from 'react';
import LegislationSigTable from '../Components/LegislationSigTable'
import LegislationSponsorTable from '../Components/LegislationSponsorTable'
import CandidateActionList from '../Components/CandidateActionList'
import { Container, Row, Table, Col } from 'react-bootstrap';
import '../Styles/LegislationInstance.css'

class LegislationInstance extends Component {
    constructor(props){
        super(props);
        this.state = {
            unpackedSponsors : [],
            isLoaded : null
        }
    }

    componentDidMount(){
        //handle splitting apart sponsordata for SponsorList here
        var split_sponsor = this.props.votesmart_bills.sponsors.split("_");
        this.state.unpackedSponsors = split_sponsor.slice(0, 25).map(data => {
            let values = data.split(/\.\d+\./);
            return {"name" : values[0], "status" : values[1], "votesmart_id" : data.slice(
                data.search(/\.\d/) + 1, data.search(/\d\./) + 1
            )}
        });
        this.setState({isLoaded : true});
    }
 

    render() {
        if(this.state.isLoaded === null){
            return(
                <div>
                    ... Loading ...
                </div>
            );
        }

        return(
            <Container> 
                <Row className="justify-content-md-center">
                    <h2> Bill "{this.props.votesmart_bills.bill_number}" </h2>
                </Row>
                <Row className="justify-content-md-center">
                    <Col sm={8}>
                        <Table striped bordered>
                        <thead>
                            <tr>
                                <th> Subject </th>
                                <th> Information </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> Bill Title </td>
                                <td> {this.props.votesmart_bills.title} </td>
                            </tr>
                            <tr>
                                <td> Date Introduced </td>
                                <td> {this.props.votesmart_bills.date_introduced} </td>
                            </tr>
                            <tr>
                                <td> Bill Type </td>
                                <td> {this.props.votesmart_bills.bill_type} </td>
                            </tr>
                            <tr>
                                <td> Number of Stages </td>
                                <td> {this.props.unpackedActionIds.length} </td>
                            </tr>
                        </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row className="justify-content-md-center">
                    <Col > 
                        <h3>
                            Bill Stages
                        </h3>
                        <CandidateActionList    actionList={this.props.unpackedActionIds} 
                                                candidateNames={this.props.candidateNames} 
                                                candidateParties={this.props.candidateParties}/>
                    </Col>
                </Row>
                <Row className="justify-content-md-center"> 
                    <Col>
                        <h3>
                            Candidate Sponsors
                        </h3>
                        <div className="scrollable_table sticky-header">
                            <LegislationSponsorTable sponsorList={this.state.unpackedSponsors} />
                        </div>
                    </Col>
                    <Col sm={8}>
                        <h3>
                            Related Interest Groups
                        </h3>
                        <div className="scrollable_table sticky-header">
                            <LegislationSigTable sigList={this.props.sigList} />
                        </div>
                    </Col>
                </Row>    
            </Container>
        )
    }
}

export default LegislationInstance;