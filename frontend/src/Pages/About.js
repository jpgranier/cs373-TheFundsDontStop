import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {Container, Row, Col, Image, Button, Table, Card} from 'react-bootstrap';
import Jonathan from '../Assets/jonathan.jpg'
import Timothy from '../Assets/timothy.jpg'
import Randy from '../Assets/randy.jpg'
import Roger from '../Assets/roger.jpg'
import Rithvik from '../Assets/rithvik.jpg'
import Mahir from '../Assets/mahir.jpg'
import Gitlab from '../Assets/gitlab.png'
import Postman from '../Assets/postman.png'
import ReactImg from '../Assets/react.png'
import BootstrapImg from '../Assets/bootstrap.png'
import AWSImg from '../Assets/aws.png'
import GitlabImg from '../Assets/gitlab-icon-rgb.png'
import PostmanImg from '../Assets/postman-logo-2.png'
import SeleniumImg from '../Assets/selenium.png'
import MochaImg from '../Assets/mocha.png'
import Ballotpedia from '../Assets/ballotpedia.png'
import Opensecrets from '../Assets/opensecrets.png'
import Propublica from '../Assets/propublica.png'
import Votesmart from '../Assets/votesmart.png'
import '../Styles/About.css';


const PROJECT_ID = 17015351;


export default class About extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          team: {

            'randizzleDee': {
              name: 'Randy Donaldson',
              commitID: ['Randy Donaldson'],
              issues: [],
            },


            'timgan': {
              name: 'Timothy Gan',
              commitID: ['Timothy Gan'],
              issues: [],
              },


            'jpgranier': {
              name: 'Jonathan Granier',
              commitID: ['Jonathan Granier'],
              issues: [],
              },


            'mahirkarim': {
              name: 'Mahir Karim',
              commitID: ['Mahir Karim'],
              issues: [],
              },


            'rith1': {
              name: 'Rithvik Vellaturi',
              commitID: ['Rithvik Vellaturi'],
              issues: [],
              },


            'Lapithes': {
              name: 'Roger Zhong',
              commitID: ['Roger Zhong'],
              issues: [],
              },

          },
          commits: [],
          totalCommits: 0,
          totalIssues: 0,
          isLoaded: null
        };
      }

    componentDidMount() {

        let usernames = ["randizzleDee", "Lapithes", "rith1", "mahirkarim", "timgan", "jpgranier"]

        for(let i=0; i<usernames.length; i++){
          fetch(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues_statistics?assignee_username=` + usernames[i])
          .then(res => res.json())
          .then(data => {
            let tempdict = this.state.team
            tempdict[usernames[i]].issues = data.statistics.counts
            this.setState({team : tempdict})
          })
        }

        fetch(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues_statistics`)
        .then(res => res.json())
        .then((data) => {
          this.setState({ totalIssues: data.statistics.counts})
        })
        .catch(console.log)

        fetch(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/contributors`)
        .then(res => res.json())
        .then(data => {
            const stats = Array.from(data)
            this.setState({commits: stats})
            this.setState({isLoaded: true})
        })
        .catch(console.log)

      }


      render(){

        const {
            team,
            commits,
            totalIssues,
          } = this.state;

        let issuesMap = {
          "Randy": team['randizzleDee'].issues.all,
          "Timothy": team['timgan'].issues.all,
          "Jonathan": team['jpgranier'].issues.all,
          "Mahir": team['mahirkarim'].issues.all,
          "Rithvik": team['rith1'].issues.all,
          "Roger": team['Lapithes'].issues.all,
      };


        let commitsMap = {
          "randy": 0,
          "Timothy Gan": 0,
          "Jonathan Granier": 0,
          "mahirkarim": 0,
          "rithvik": 0,
          "Roger Zhong": 0
        };

        var totalCommits = 0

        commits.map((commit, ) => {
          commitsMap[commit.name] += commit.commits
          totalCommits += commit.commits
       })

          
          return(
            <React.Fragment>
            <h1>About Us</h1>
            <h4>
            Our idea was to make a website dedicated to tracking the legislative process through the lenses of interest group support and campaign contributions. 
            Interest groups help shape legislation through lobbying officials and supporting sympathetic candidates, so we believe it is important to be aware of who contributes to a campaign to fully understand what policy issues the candidate will prioritize if elected.
             The Funds Don’t Stop shows this convergence of donors, lobbyists, and candidates on policy positions and legislation.
            </h4>
            <h4>
            Our mission is to provide you with the information you need to make informed choices. 
            </h4>
            <br></br>
            <br></br>
            <h1>Our Team</h1>

            <Container>

            <Row className="show-grid text-center">
                <Col xs={12} sm={4} className="person-wrapper">
                <Card className="card h-100 mb-4">
                  <Card.Img src={Jonathan} variant="top" />
                    <Card.Body>
                      <Card.Title>Jonathan Granier (Backend/Database)</Card.Title>
                      <Card.Text>
                      <p>
                        <li>Senior in college splitting his time between an internship and school</li>
                        <li>I’ve done backend web development for over a year at Proofpoint</li>
                      </p>
                      <p>
                        Role: Managed backend, database, and webapp deployment
                      </p>
                      </Card.Text>
                    </Card.Body>
                </Card>

                </Col>
                <Col xs={12} sm={4} className="person-wrapper">
                <Card className="card h-100 mb-4">
                  <Card.Img src={Rithvik} variant="top" />
                    <Card.Body>
                      <Card.Title>Rithvik Vellaturi (Frontend/API)</Card.Title>
                      <Card.Text>
                      <p>
                        <li>CS major at UT Austin</li>
                        <li>Enjoys tennis, video games, and hiking</li>
                      </p>
                      <p>
                        Role: Gathered data on candidates, frontend developer for candidate instance pages
                      </p>
                      </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} sm={4} className="person-wrapper">
                <Card className="card h-100 mb-4">
                  <Card.Img src={Randy} variant="top" />
                    <Card.Body>
                      <Card.Title>Randy Donaldson (Backend/API)</Card.Title>
                      <Card.Text>
                      <p>
                        <li>I am a Junior in CS</li>
                        <li>My hobbies include bouldering, fantasy football, and watching movies</li>
                      </p>
                      <p>
                        Role: Gathered data on interest groups, responsible for routing endpoints in Flask
                      </p>
                      </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} sm={4} className="person-wrapper">
                <Card className="card h-100 mb-4">
                  <Card.Img src={Timothy} variant="top" />
                    <Card.Body>
                      <Card.Title>Timothy Gan (Frontend/API)</Card.Title>
                      <Card.Text>
                      <p>
                        <li>Junior in CS</li>
                        <li>Drinks yogurt daily</li>
                        <li>Volleyball nerd</li>
                      </p>
                      <p>
                        Role: Gathered data on legislation, frontend developer for legislation instance pages
                      </p>
                      </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} sm={4} className="person-wrapper">
                <Card className="card h-100 mb-4">
                  <Card.Img src={Roger} variant="top" />
                    <Card.Body>
                      <Card.Title>Roger Zhong (Frontend/Testing)</Card.Title>
                      <Card.Text>
                      <p>
                        <li>Sophomore CS major at UT Austin</li>
                        <li>Enjoys playing golf and fishing</li>
                      </p>
                      <p>
                        Role: Updated splash/about page, frontend developer for interest group instance pages
                      </p>
                      </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} sm={4} className="person-wrapper">
                <Card className="card h-100 mb-4">
                  <Card.Img src={Mahir} variant="top" />
                    <Card.Body>
                      <Card.Title>Mahir Karim (Full Stack)</Card.Title>
                      <Card.Text>
                      <p>
                        <li>A senior CS and Government double major, his thirst for knowledge is only exceeded by his hunger for small ferns</li>
                        <li>He has one ambition: owning more magnets</li>
                      </p>
                      <p>
                        Role: Implemented pagination on backend, developed routing and model pages on frontend
                      </p>
                      </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
            </Row>
            </Container>

            <h2>
                Source code and API
            </h2>
            <Container>
            <Row>
              <Col>
              <Image src={Gitlab} width="400px"/>
              <h3>
              <a href={"https://gitlab.com/jpgranier/cs373-TheFundsDontStop"}>GitLab</a>
              </h3>
              </Col>
              <Col>
              <Image src={Postman}width="400px"/>
              <h3>
              <a href={"https://documenter.getpostman.com/view/10505716/SzfDxR5Q"}>Postman</a>
              </h3>
              </Col>
            </Row>
            </Container>


            <br></br>
            <h2>
                Data Sources
            </h2>
            <h3>
            <Image src={Votesmart}className="logo" width="200px"/>
            <a href={"https://votesmart.org/elections"}>votesmart</a>
            &emsp;
            <Image src={Propublica}className="logo" width="200px"/>
            <a href={"https://www.propublica.org/"}>propublica</a>
            </h3>
            <h3>

            </h3>
            <h3>
            <Image src={Opensecrets}className="logo" width="200px"/>
            <a href={"https://www.opensecrets.org/"}>opensecrets</a>
            &emsp;
            <Image src={Ballotpedia}className="logo" width="200px"/>
            <a href={"https://ballotpedia.org/Main_Page"}>ballotpedia</a>
            </h3>


            <br></br>
            <h2>
                Tools:
            </h2>
            <p><Image src={ReactImg}className="logo" width="80px"/> React: Frontend JS framework.</p>
            <p><Image src={BootstrapImg}className="logo" width="80px"/> Bootstrap: Frontend component library and toolkit.</p>
            <p><Image src={AWSImg}className="logo" width="80px"/> AWS: Cloud computing platform that hosts our website.</p>
            <p><Image src={GitlabImg}className="logo" width="80px"/> GitLab: Workflow and Continuous Integration.</p>
            <p><Image src={PostmanImg}className="logo" width="80px"/> Postman: API design.</p>
            <p><Image src={SeleniumImg}className="logo" width="80px"/> Selenium: GUI tests.</p>
            <p><Image src={MochaImg}className="logo" width="80px"/> Mocha: Frontend JS unit testing.</p>


            <br></br>
            <div>
            <center><h1>Repository Statistics</h1></center>

              <div className="card">
                <div className="card-body">
            <div className="center">
              <h5>
              Commits: {totalCommits}&emsp;Issues Closed/Total: {totalIssues.closed}/{totalIssues.all}&emsp; Tests: {158}
              </h5>
            </div>
                </div>
              </div>
            </div>

            
              <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Commits</th>
                        <th>Issues</th>
                        <th>Tests</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    <tr>
                      <td>{'Randy'}</td>
                      <td>{commitsMap['randy']}</td>
                      <td>{issuesMap['Randy']}</td>
                      <td>{60}</td>
                    </tr>

                    <tr>
                      <td>{'Timothy'}</td>
                      <td>{commitsMap['Timothy Gan']}</td>
                      <td>{issuesMap['Timothy']}</td>
                      <td>{11}</td>
                    </tr>

                    <tr>
                      <td>{'Jonathan'}</td>
                      <td>{commitsMap['Jonathan Granier']}</td>
                      <td>{issuesMap['Jonathan']}</td>
                      <td>{40}</td>
                    </tr>

                    <tr>
                      <td>{'Mahir'}</td>
                      <td>{commitsMap['mahirkarim']}</td>
                      <td>{issuesMap['Mahir']}</td>
                      <td>{30}</td>
                    </tr>

                    <tr>
                      <td>{'Rithvik'}</td>
                      <td>{commitsMap['rithvik']}</td>
                      <td>{issuesMap['Rithvik']}</td>
                      <td>{5}</td>
                    </tr>

                    <tr>
                      <td>{'Roger'}</td>
                      <td>{commitsMap['Roger Zhong']}</td>
                      <td>{issuesMap['Roger']}</td>
                      <td>{12}</td>
                    </tr>
                    </tbody>

              </Table>

        
            </React.Fragment>

          )

      }
}



