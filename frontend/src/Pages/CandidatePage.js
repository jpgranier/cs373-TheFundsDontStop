import React, {Component} from 'react';
import Candidate_Info from '../Components/Candidate_Info.js'
import Candidate_FinancialContributions from '../Components/Candidate_FinancialContributions.js'
import Candidate_Ratings from '../Components/Candidate_Ratings.js'
import Candidate_BillDecisions from '../Components/Candidate_BillDecisions.js'
import 'bootstrap/dist/css/bootstrap.min.css';

class CandidatePage extends Component{
  render(){
  return (
    <div className="CandidatePage">
      
      <Candidate_Info  votesmart_id={this.props.match.params.votesmart_id} />
      <br></br>
      <h3>Top Industries</h3>
      <Candidate_FinancialContributions votesmart_id={this.props.match.params.votesmart_id}/>
      <br></br>
      <h3>Candidate Ratings</h3>
      <Candidate_Ratings votesmart_id={this.props.match.params.votesmart_id}/>
      <br></br>
      <h3>Votes on Bills</h3>
      <Candidate_BillDecisions votesmart_id={this.props.match.params.votesmart_id}/>
    </div>
  );
  }
}

export default CandidatePage;