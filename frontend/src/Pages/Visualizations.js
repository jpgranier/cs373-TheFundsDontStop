
import React, { Component } from 'react';
import ClimateVisual from './../Components/ClimateVisual'
import { Container, Tabs, Tab, Row } from 'react-bootstrap';
import Candidate_vis from '../Components/Candidate_vis';
import IG_vis from '../Components/IG_vis';
import LG_vis from '../Components/LG_vis';

class Visualizations extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }

    render(){
        return(
            <Container>

                <Row className="justify-content-md-center">
                    <h2>Climate Buddy Visualizations</h2>
                </Row>

                <Row>
                    <ClimateVisual/>
                </Row>

                <Row className="justify-content-md-center">
                    <h2> The Funds Don't Stop Visualizations</h2>
                </Row>
                       
                <Candidate_vis />

                <IG_vis/>

                <LG_vis/>
                
            </Container>
        )
    }
}

export default Visualizations;