import React, {Component} from 'react'
import bb from 'billboard.js'

class Candidate_vis extends Component {

    state = {
        info : null,
        barchart : null
    }
    

    componentDidMount() {
		fetch("https://api.thefundsdontstop.me/getAllCandidateInfo")
		.then((res) => res.json())
		.then(data =>{
            this.setState({info : data})           			
        })
        .then(finished => {
            var d = 0;
            var r = 0;
            var i = 0;
            var c;
            for (c = 0; c < this.state.info.length; c++) {
                if (this.state.info[c].party === 'D') {
                    d++;
                }
                else if (this.state.info[c].party === 'R') {
                    r++;
                }
                else if (this.state.info[c].party === 'ID') {
                    i++;
                }
            }
            var chart = bb.generate({
                data: {
                  columns: [
                  ["Democrat", d],
                  ["Republican", r],
                  ["Independent", i]
                  ],
                  type: "bar"
                },
                axis: {
                    x: {
                        label: "Party"
                    },
                    y: {
                        label: "Number of Candidates"
                    }
                },
                bar: {
                  width: {
                    ratio: 0.5
                  }
                },
                
              });
            this.state.barchart = chart						
        })
    }
    
	render() {
		return (
			<div>
                {this.state.barchart}
            </div>
        )
	}
    
}

export default Candidate_vis;