import React, {Component} from 'react'
import bb from 'billboard.js'

class IG_vis extends Component {

    state = {
        info : null,
        barchart : null
    }
    componentDidMount() {
		fetch("https://api.thefundsdontstop.me/getAllInterestGroups")
		.then((res) => res.json())
		.then(data =>{
            this.setState({info : data})           			
        })
        .then(finished => {
            var pac = 0;
            var cand = 0;
            var c;
            for (c = 0; c < this.state.info.length; c++) {
                if (this.state.info[c].gave_to_cand != -1) {
                    cand += this.state.info[c].gave_to_cand
                }
                if (this.state.info[c].gave_to_pac != -1) {
                    pac += this.state.info[c].gave_to_pac
                } 
            }
            var chart = bb.generate({
                data: {
                  columns: [
                  ["PAC", pac],
                  ["Candidate", cand],
                  ],
                  type: "bar"
                },
                axis: {
                    x: {
                        label: "Contribution to"
                    },
                    y: {
                        label: "$"
                    }
                },
                bar: {
                  width: {
                    ratio: 0.5
                  }
                },
                
              });
            this.state.barchart = chart						
        })
    }
    
	render() {
		return (
			<div>
                {this.state.barchart}
            </div>
        )
	}
    
}

export default IG_vis;