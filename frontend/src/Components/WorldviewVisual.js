import React, { Component } from 'react';
import { Container, Row, Table, Col } from 'react-bootstrap';
import '../Styles/LegislationInstance.css'
import * as d3 from 'd3'

class WorldviewVisual extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoaded: null
        }
        this.zoom = this.zoom.bind(this);
        this.xAxis = this.xAxis.bind(this);
        this.yAxis = this.yAxis.bind(this);
        this.drawChart = this.drawChart.bind(this);

    }

    componentDidMount(){
        this.setState({isLoaded: true})
        this.drawChart()
        console.log(this.props.worldviews)
    }

    //code adapted from D3's zoomable bar chart

    zoom(svg) {
        const extent = [[this.props.margin.left, this.props.margin.top], [this.props.width - this.props.margin.right, this.props.height - this.props.margin.top]];
        svg.call(d3.zoom()
            .scaleExtent([1, 8])
            .translateExtent(extent)
            .extent(extent)
            .on("zoom", () => {
                this.x.range([this.props.margin.left, this.props.width - this.props.margin.right].map(d => d3.event.transform.applyX(d)));
                svg.selectAll(".bars rect").attr("x", d => this.x(d.name)).attr("width", this.x.bandwidth());
                svg.selectAll(".x-axis").call(this.xAxis);
            }));
    }

    x = d3.scaleBand()
        .domain(this.props.worldviews.map(d => d.name))
        .range([this.props.margin.left, this.props.width - this.props.margin.right])
        .padding(0.1)

    y = d3.scaleLinear()
        .domain([0, d3.max(this.props.worldviews, d => d.value)]).nice()
        .range([this.props.height - this.props.margin.bottom, this.props.margin.top])

    xAxis = g => g
        .attr("transform", `translate(0,${this.props.height - this.props.margin.bottom})`)
        .call(d3.axisBottom(this.x).tickSizeOuter(0))

    yAxis = g => g
        .attr("transform", `translate(${this.props.margin.left},0)`)
        .call(d3.axisLeft(this.y))
        .call(g => g.select(".domain").remove())

    drawChart(){
        const svg = d3.select('div.worldview').append("svg")
                .style("width", '1400px')
                .style("height", '700px')
                .attr("viewBox", [0, 0, this.props.width, this.props.height])
                .call(this.zoom);
        
        svg.append("g")
            .attr("class", "bars")
            .attr("fill", "steelblue")
            .selectAll("rect")
            .data(this.props.worldviews)
            .join("rect")
            .attr("x", d => this.x(d.name))
            .attr("y", d => this.y(d.value))
            .attr("height", d => this.y(0) - this.y(d.value))
            .attr("width", this.x.bandwidth());
        
        svg.append("g")
            .attr("class", "x-axis")
            .call(this.xAxis);
        
        svg.append("g")
            .attr("class", "y-axis")
            .call(this.yAxis);   
    }   

    render() {
        return(
            <Container> 
                <Row className="justify-content-md-center">
                    <div >
                        Worldview Visualization: Coefficient of Variation
                    </div>
                    <div class="worldview">
                        
                    </div>
                    
                    
                </Row>
            </Container>
        )
        
    }
}

export default WorldviewVisual;