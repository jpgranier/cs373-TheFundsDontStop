import React from 'react';
import {Nav, Container, Col, Row, Table} from 'react-bootstrap'
import '../Styles/InterestGroupInstance.css'

class IG_CandidateRatings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        c_ratings: [{},],
        c_ratings1: {},
        candidateNames : null,
        formVal: "",
        isLoaded : null
      }

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    //const { ratings } = this.props;

    fetch("https://api.thefundsdontstop.me/getCandidateIds") //relating votesmart_id with candidate name
    .then(data => data.json())
    .then(data => {
        var candidateDicts = data;
        var candMap = {}
        candidateDicts.forEach((candidateDict) => {
            candMap[candidateDict["candidate_votesmart_id"]] = candidateDict["candidate_name"];
        });
        this.setState({candidateNames : candMap});
        this.setState({isLoaded : true})
    })
    .catch(console.log);


  }

  async search() {
    this.setState({isLoaded: true});
    }

  handleChange(event){
      this.state.formVal = event.target.value
      this.search()
   }

  handleSubmit(event) {
    this.setState({isLoaded:false})
    this.search()
    this.setState({isLoaded:true})
    event.preventDefault();
 }

  displayRatings = () => {
    const { ratings } = this.props

    if(ratings == null){
      return(<React.Fragment>
        <h4>This interest group has no ratings.</h4>
        </React.Fragment>)
    }

    if(Object.keys(ratings).length == 0){
      return(<React.Fragment>
        <h4>This interest group has no ratings.</h4>
        </React.Fragment>)
    }

    let rows = [];
    

    rows = ratings.map((rating, ) => {

    let cand_name = this.state.candidateNames[rating.candidate_votesmart_id]
    if(cand_name == null)
      cand_name = '<UNLISTED>'
    if(cand_name.toLowerCase().search(this.state.formVal.toLowerCase()) == -1)
      return(null)
    return(
        <React.Fragment>
        <tr>
            <td>{<Nav.Link href={"/candidate/" + rating.candidate_votesmart_id }>
          <p>{cand_name}</p></Nav.Link>}</td>
            <td>{rating.rating}</td>
            <td>{rating.timespan}</td>
        </tr>

        </React.Fragment>
    )
})


    return rows;
  };

  render() {

    if (this.state.isLoaded === null){
      return (<div></div>)
    }

    return (

      <React.Fragment>
        <form onSubmit={this.handleSubmit}>
        <label>
          <input type="text" value={this.state.formVal} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Search Name" />
        </form>

        <div class="scrollable_table sticky-header">
        <Table striped bordered hover>
        <thead>
            <tr>
                <th>Name</th>
                <th>Rating</th>
                <th>Timespan</th>
            </tr>
        </thead>
          <tbody>
            {this.displayRatings()}
          </tbody>
        </Table>
        </div>
      </React.Fragment>
    );
  }
}

export default IG_CandidateRatings;