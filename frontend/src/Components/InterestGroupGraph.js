import React from 'react';
import {Container, Table} from 'react-bootstrap'
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts' 

class InterestGroupGraph extends React.Component {
  render() {
    const { interest } = this.props;
    let {
      name, description, address, phone, dems, repubs, gave_to_pac, gave_to_cand, total
    } = interest;

    var dataMissing = false
    if (dems == -1 || repubs == -1 || gave_to_pac == -1 || gave_to_cand == -1)
      dataMissing = true

    if(dataMissing){
        return(<React.Fragment>
            <h4>Missing data needed for charts.</h4>
        </React.Fragment>)
    }

    const data1 = [
        {
          name: 'Democrats', 'contribution_$': dems
        },
        {
          name: 'Republicans', 'contribution_$': repubs
        },

    ]

    const data2 = [
        {
          name: 'PACs', 'contribution_$': gave_to_pac
        },
        {
          name: 'Candidates', 'contribution_$': gave_to_cand
        },

    ]


    return (
      <Container>
        <div>
        <h5>Contributions by Party</h5>
        <BarChart
            width={300}
            height={200}
            data={data1}
            margin={{
            top: 5, right: 0, left: 20, bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Bar dataKey="contribution_$">
            <Cell key={`cell-0`} fill="blue" />
            <Cell key={`cell-1`} fill="red" />
            </Bar>
        </BarChart>
        </div>

        <div>
        <h5>Contributions by Type</h5>
        <BarChart
            width={300}
            height={200}
            data={data2}
            margin={{
            top: 5, right: 0, left: 20, bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip/>
            <Bar dataKey="contribution_$">
            <Cell key={`cell-0`} fill="purple" />
            <Cell key={`cell-1`} fill="green" />
            </Bar>
        </BarChart>
        </div>

      </Container>
    );
  }
}

export default InterestGroupGraph;
