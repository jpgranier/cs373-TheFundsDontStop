import React, { Component } from 'react';
import {Table} from 'react-bootstrap';

// takes in siglist into properties, should be list of sig

class LegislationSponsorTable extends Component{
    render() {
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th> Sponsor Name </th>
                        <th> Primary Sponsor or Co-Sponsor </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.sponsorList.map((sponsor) => (
                            <tr key={sponsor.name}>
                                <td>
                                    {sponsor.name}       
                                </td>
                                <td>
                                    {sponsor.status}
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        );
    }
}

export default LegislationSponsorTable;