import React, {Component} from 'react'
import {Card,Button, Container, Row, Col} from 'react-bootstrap'
import { TwitterTimelineEmbed} from 'react-twitter-embed';

function Tw_display(prop) {
	if (prop.twacc) {
		return (
			<div>
			<div className="centerContent">
			<div className="selfCenter standardWidth">
			<TwitterTimelineEmbed
			sourceType="profile"
			screenName= {prop.twacc}
			options={{height: 600, width: 600}}
			/>
			</div>
			</div>			
			</div>
		);
	}
	else {
		return (
			<div>
				<b>No Twitter account found.</b>
			</div>
		);
	}
}

function Fb_display(prop) {
	if (prop.fbacc) {
		return (
			<div>
			<Button href = {"https://www.facebook.com/" + prop.fbacc}> Like on Facebook</Button>
			</div> 
		);
	}
	else {
		return (
			<div>
				<b> No Facebook account found.</b>
			</div>
		);
	}
}


class Candidate_Info extends Component {
	
	state = {
		isLoaded : null,
		info : null
	}
	
	componentDidMount() {
		fetch("https://api.thefundsdontstop.me/getContactInfoByVoteSmartId?votesmart_id=" + this.props.votesmart_id)
		.then((res) => res.json())
		.then(data =>{
			this.setState({info : data})
			//console.log(data)
			
		})
		.then(finished => {
			if (this.state.info.district === null) {
				this.state.info.district = 'N/A'
			}
			this.setState({isLoaded : true})		
		})
	}
	render() {
		if (this.state.isLoaded === null) {
			return (
				<div>
					<b> Loading </b>
				</div>
			)
		}
		return (
			<Row>
			<Col md="auto">
		<Card style={{ width: '16rem' }}>
		<Card.Img variant="top" src= {this.state.info.image_url} />
		<Card.Body>
		  <Card.Title>{this.state.info.first_name} {this.state.info.last_name}</Card.Title>
		  <Card.Text>
			Party: {this.state.info.party}
		  </Card.Text>
		  <Card.Text>
			District: {this.state.info.district}
		  </Card.Text>
		  <p><Button href={this.state.info.website_url}>Candidate website</Button></p>			  
		  <p><Fb_display fbacc = {this.state.info.facebook_account}/></p>
		</Card.Body>
	  </Card>
	  </Col>
	  <Col md = "auto">
	  <Tw_display twacc={this.state.info.twitter_account} />
		</Col>
		</Row>
			 );
	}
}

export default Candidate_Info;