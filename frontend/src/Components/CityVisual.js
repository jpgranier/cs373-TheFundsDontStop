import React, { Component } from 'react';
import { Container, Row } from 'react-bootstrap';
import '../Styles/LegislationInstance.css'
import * as d3 from 'd3'
class CityVisual extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoaded : null,
        }
        this.z = this.z.bind(this)
        this.xAxis = this.xAxis.bind(this)
        this.yAxis = this.yAxis.bind(this)
        this.drawChart = this.drawChart.bind(this)
    }

    // code adapted from D3's example temperature scatterplot graph
    componentDidMount(){
        this.setState({isLoaded: true})
        console.log(this.props.cities)
        this.drawChart()
    }

    x = d3.scaleLinear()
        .domain(d3.extent(this.props.cities, d => d.green)).nice()
        .range([this.props.margin.left, this.props.width - this.props.margin.right])


    y = d3.scaleLinear()
            .domain(d3.extent(this.props.cities, d => d.aqi)).nice()
            .range([this.props.height - this.props.margin.bottom, this.props.margin.top])

    z = (val)=>{
            const max = d3.max(this.props.cities, d => Math.abs(d.aqi));
            return d3.scaleSequential(d3.interpolateRdBu).domain([max, -max])(val);
    }

    xAxis = g => g
        .attr("transform", `translate(0,${this.props.height - this.props.margin.bottom})`)
        .call(d3.axisBottom(this.x).ticks(this.props.width / 80))
        .call(g => g.select(".domain").remove())

    yAxis = g => g
        .attr("transform", `translate(${this.props.margin.left},0)`)
        .call(d3.axisLeft(this.y))
        .call(g => g.select(".domain").remove())

    drawChart(){
        const svg = d3.select('div.city')
                .append("svg")
                .style("width", '1000px')
                .style("height", '700px')
            .attr("viewBox", [0, 0, this.props.width, this.props.height]);

        svg.append("g")
            .call(this.xAxis);

        svg.append("g")
            .call(this.yAxis);

        svg.append("g")
            .attr("stroke", "#000")
            .attr("stroke-opacity", 0.2)
            .selectAll("circle")
            .data(this.props.cities)
            .join("circle")
            .attr("cx", d => this.x(d.green))
            .attr("cy", d => this.y(d.aqi))
            .attr("fill", d => this.z(d.aqi))
            .attr("r", 2.5);
    }
    
    render() {
        
        if(this.props.isLoaded === null){
            return(
                <div>
                    ... Loading ...
                </div>
            )
        }
        
        return(
            <Container> 
                <Row className="justify-content-md-center">
                    <div >
                        City Visualization: Greenness vs Air Quality Index
                    </div>
                    <div class="city">
                        
                    </div>
                </Row>
            </Container>
        )
    }
}

export default CityVisual;