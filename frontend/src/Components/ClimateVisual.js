
import React, { Component } from 'react';
import CityVisual from './CityVisual';
import CountryVisual from './CountryVisual';
import WorldviewVisual from './WorldviewVisual';
import { Container, Tabs, Tab } from 'react-bootstrap';

class ClimateVisual extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoaded : null,
            countries: null,
            cities: null,
            worldviews: null,
        }
    }

    componentDidMount(){
        fetch("https://api.climatebuddy.xyz/countries")
        .then((res) => {
            return res.json()})
        .then((data) => {
            var countryData = data.map((country) => {
                let gdp = country["GDPCapita"] ? country["GDPCapita"]: 0
                let emi = country["emissionsCapita"] ? country["emissionsCapita"]: 0
                return {gdp : gdp,  emi : emi }
            })
            this.setState({countries : countryData})
        })
        .then(() =>{
            return fetch("https://api.climatebuddy.xyz/cities")
        })
        .then((res) => {
            return res.json()
        })
        .then((data) => {
            var cityData = data.map((city) => {
                let green = city["percent_greenness"] ? city["percent_greenness"]: 0
                let aqi = city["AQI"] ? city["AQI"]: 0
                return {green : green,  aqi : aqi }
            })
            this.setState({cities : cityData})
        })
        .then(() =>{
            return fetch("https://api.climatebuddy.xyz/stats")
        })
        .then((res) => {
            return res.json()
        })
        .then((data) => {
            var worldviewData = data.map((worldview) => {
                return {name : worldview["name"], value : worldview["stdDev"]/worldview["mean"] }
            })
            this.setState({worldviews : worldviewData})  
        })
        .then(() =>{
            this.setState({isLoaded : true})
        })
        .catch(console.log);
        console.log('hi')
    }

    
    render(){
        if(this.state.isLoaded === null){
            return(
                <p> 
                    ... Loading ...
                </p>
            );
        }
        return (
            <Container> 
                <Tabs defaultActiveKey="WorldView" id="ClimateVisuals">
                    <Tab eventKey='WorldView' title='WorldView'>
                        <WorldviewVisual    worldviews={this.state.worldviews} 
                                            margin={{top: 30, right: 30, bottom: 30, left: 30}}
                                            height={500}
                                            width={1600}/>
                    </Tab>
                    <Tab eventKey='Country' title="Country">
                        <CountryVisual  countries={this.state.countries}
                                        margin={{top: 30, right: 30, bottom: 30, left: 30}}
                                        height={500}
                                        width={1000}/>
                    </Tab>
                    <Tab eventKey='City' title="City">
                        <CityVisual cities={this.state.cities}
                                    margin={{top: 30, right: 30, bottom: 30, left: 30}}
                                    height={500}
                                    width={1000}/>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
    
}

export default ClimateVisual;