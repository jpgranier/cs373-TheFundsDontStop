import React, { Component } from 'react';
import { Pagination } from 'react-bootstrap';

//pagination component, just need to pass in methods for button types, e.g.
//                                                                      first
//                                                                      last
//                                                                      next
//                                                                      prev
//                                                                      prevEll
//                                                                      nextEll
//                                                                      selectPage
class ModelPagination extends Component{
    constructor(props){
        super(props);
    }

    render(){
        let pages = []
        //pagination will always have a max of 5 elements
        let i = Math.max(1, this.props.current - 2);
        const end = Math.min(this.props.size, this.props.current + 2)
        for(i; i <= end; ++i){
            const pageNum = i
            pages.push(<Pagination.Item active={i==this.props.current} onClick={() => {this.props.selectPage(pageNum)}}>
                   {i}
                </Pagination.Item>) 
        }
        const prevButtonDisabled = this.props.current < 4 
        const nextButtonDisabled = this.props.current > this.props.size - 3 
        return (
            <Pagination>
                <Pagination.First onClick={() => {this.props.first()}}/>
                <Pagination.Prev disabled={prevButtonDisabled} onClick={() => {this.props.prev()}}/>
                {this.props.current > 3 &&
                    <Pagination.Ellipsis onClick={() => {this.props.prevEll()}}/>
                }
                {
                    pages
                }
                {this.props.current < this.props.size - 2 &&
                    <Pagination.Ellipsis onClick={() => {this.props.nextEll()}}/>
                }
                <Pagination.Next disabled={nextButtonDisabled} onClick={() => {this.props.next()}}/>
                <Pagination.Last onClick={() => {this.props.last()}}/>
            </Pagination>
        )
    }
}


export default ModelPagination;