import React from 'react';
import { Fragment } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from './Pages/Home';
import About from './Pages/About';
import Candidates from './Pages/Candidates';
import Legislation from './Pages/Legislation';
import interests from './Pages/interests';
import {Contact} from './Contact';
import {NoMatch}  from './NoMatch';
import {Layout} from './Components/Layout';
import {NavigationBar} from './Components/NavigationBar';
import InterestGroupInstance from './Pages/InterestGroupInstance'
import CandidatePage from './Pages/CandidatePage'
import LegislationMount from './Pages/LegislationMount'
import Visualizations from './Pages/Visualizations'
import 'bootstrap/dist/css/bootstrap.min.css';




export default class App extends React.Component {


  render() {

    return(
      <React.Fragment>
        <NavigationBar />
        

          <Router>
            <Switch>
              <Route exact path="/" component={Home} />
              <Layout>
              <Route path="/about" component={About} />
              <Route path="/candidates" component={Candidates} />
              <Route path="/legislation" component={Legislation} />
              <Route path="/interests" component={interests} />
              <Route path= "/candidate/:votesmart_id" render={(props) => <CandidatePage {...props}/>}/>
              <Route path= "/interest_groups/:votesmart_id" render={(props) => <InterestGroupInstance {...props}/>}/>
              <Route path= "/bills/:bill_id" render={(props) => <LegislationMount {...props}/>}/>
              <Route path="/test" component={InterestGroupInstance}/>
              <Route path="/visualizations" component={Visualizations}/>
              {/* <Route path={'/bills/:bill_id'} component={LegislationMount}/>  */}
              
              </Layout>
              <Route component={NoMatch} />
            </Switch>
          </Router>

      </React.Fragment>
    )
  }
}
