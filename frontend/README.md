Docker Instructions:

cd /frontend
docker build -t thefundsdontstop-client:latest .
docker run -d -p 3000:3000 thefundsdontstop-client

Visit localhost

Or alternatively,

chmod +x start
./start


click refresh until page shows up
