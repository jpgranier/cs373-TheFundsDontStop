import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {expect, should} from 'chai';


import InterestGroupInstance from '../src/Pages/InterestGroupInstance';
import IG_BillsByCategory from '../src/Components/IG_BillsByCategory'
import IG_CandidateRatings from '../src/Components/IG_CandidateRatings'
import InterestGroupCard from '../src/Components/InterestGroupCard'
import InterestGroupGraph from '../src/Components/InterestGroupGraph'
import interests from '../src/Pages/interests';

var chai = require('chai');
var assert = require('assert')


configure({ adapter: new Adapter() });

describe('IG_Inst', () => {
  const wrapper = shallow(
    <InterestGroupInstance match={{params: {votesmart_id: 1086}}}/>
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

  it('is loaded', () => {
    setTimeout(() => {
      wrapper.update();
      expect(wrapper.state().isLoaded).is.true;
      expect(wrapper.find('Container').length).to.equal(1)
      expect(wrapper.find('InterestGroupGraph')).to.not.be.undefined
      expect(wrapper.find('InterestGroupCard').length).to.equal(1)
      expect(wrapper.find('IG_BillsByCategory').length).to.equal(1)
      expect(wrapper.find('IG_CandidateRatings').length).to.equal(1)
      
    }, 2000);
  });

  it('API returns', async () => {
    await fetch('https://api.thefundsdontstop.me/getInterestGroupsByVotesmartId?votesmart_id=1086')
        .then((res) => {
            return res.json()
        })
        .then((res) => {
            assert.equal(res.sig_id, 2)
        })
  })
});

describe('IG_BillsByCategory', () => {
  const wrapper = shallow(
    <IG_BillsByCategory category={31}/>
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

  it('is loaded', () => {
    setTimeout(() => {
      wrapper.update();
      expect(wrapper.state().isLoaded).is.true;
      expect(wrapper.state().bills).is.not.null;
      expect(wrapper.find('tbody').length).to.equal(1)
      
    }, 2000);
  });

});

describe('IG_CandidateRatings', () => {
  const wrapper = shallow(
    <IG_CandidateRatings ratings={0}/>
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

  it('is loaded', () => {
    setTimeout(() => {
      wrapper.update();
      expect(wrapper.state().isLoaded).is.true;
      expect(wrapper.state().candidateNames).is.not.null;
      expect(wrapper.find('tbody').length).to.equal(1)
      
    }, 2000);
  });

});


describe('InterestGroupCard', () => {

  const wrapper = shallow(
    <InterestGroupCard interest={0}/>
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

  it('is loaded', () => {
    setTimeout(() => {
      wrapper.update();
      expect(wrapper.find('tbody').length).to.equal(1)
    }, 1000);
  });

});

describe('InterestGroupGraph', () => {

  const wrapper = shallow(
    <InterestGroupGraph interest={0}/>
  );

  it('is defined', () => {
    expect(wrapper).to.not.be.undefined;
    expect(wrapper.state()).to.not.be.undefined;
  });

});




  // describe('IG_Inst', () => {
  //   var component
  //   var load_promise

  //   before(()=>{
  //     load_promise = new Promise((resolve, reject)=>{
  //       setTimeout(()=>resolve(999), 1800);
  //     });
  //     component = shallow(<InterestGroupInstance match={{params: 1086}}/>);
  //   });

  //   it('id should not be undefined', () => {
  //     return load_promise.then(() => {expect(component.state().id).to.equal(1086)})
  //     .catch((err) => {
  //       assert.fail(err)
  //     })
  //   })

  // });


  // const data = wrapper.instance();
  // expect(data.componentDidMount().props).to.equal(1086);

    // let props;
    // let component;
  
    // const div = document.createElement('div');
    // const wrappedMount = () => mountWrap(<InterestGroupInstance {...props} />, div);
  
    // beforeEach(() => {
    //   props = {
    //     id: null,
    //   };
    //   if (component) component.componentWillUnmount();
    // });
  
    // it("renders page", () => {
    //   chai.request('http://localhost:3000').get('/interest_groups/null')
    //       .end(function(err, res){
    //         expect(res == undefined).is.true
    //       });

  

  // describe('rendering', () => {
  //   let wrapper, showUrl
  //    beforeEach(() => {
  //      props = 1086
  //      showUrl = sinon.stub('fetch')
  //    })
  //    afterEach(() => {
  //      showUrl.reset()
  //    })
  //    it('ComponentDidMount', () => {
  //      wrapper = shallow(<Mycomponent  {...props}/>)
  //      expect(showUrl.calledOnce).toBe(true)
  //    })
  // })