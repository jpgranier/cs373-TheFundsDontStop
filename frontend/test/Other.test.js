import React from 'react'
import ReactDOM from 'react-dom';
import { expect } from 'chai';
import { mount, render, shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from '../src/Pages/Home';
import About from '../src/Pages/About';
describe('AboutPage', () => {
    const wrapper = shallow(<About/>);

    it('is defined', () => {
      expect(wrapper).to.not.be.undefined;
      expect(Object.keys(wrapper.state().team).length).to.equal(6);
    });

    it('is loaded', () => {
      setTimeout(() => {
        wrapper.update();
        expect(wrapper.state().isLoaded).is.true;
        expect(wrapper.containsMatchingElement(<h1>Repository Statistics</h1>)).is.true;
        // expect(wrapper.find('Container').length).to.equal(1)
        expect(wrapper.find('td').length).to.equal(24)
        // expect(wrapper.find('InterestGroupCard').length).to.equal(1)
        
      }, 2000);

    });



  });

  describe('HomePage', () => {
    it('is defined', () => {
      const wrapper = shallow(<Home/>);
      expect(wrapper).to.not.be.undefined;
      expect(wrapper.find('Image').length).to.equal(3)
    });
  });
