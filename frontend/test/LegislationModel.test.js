import React from 'react'
import { expect } from 'chai';
import { mount, render, shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Legislation from '../src/Pages/Legislation.js';

import { Row, Container, Table, Tabs, Col} from 'react-bootstrap';
import ModelPagination from '../src/Components/ModelPagination.js';


// code taken from https://github.com/enzymejs/enzyme/issues/942 to set up jsdom
var jsdom = require('jsdom');
var assert = require('assert')
const { JSDOM } = jsdom;
const { document } = (new JSDOM('')).window;



configure({ adapter: new Adapter() });
global.expect = expect;
global.mount = mount;
global.render = render;
global.shallow = shallow;
global.document = document;
global.window = document.defaultView['window']



describe('Legislation', function() {
    this.timeout(5000)
    var component = null
    var load_promise = null
    before(() => {
        load_promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(999), 2500); //wait to ensure all the data that gets loaded in from componentDidMount
        });
        component = shallow(<Legislation/>);
    });
    
    it('should have a h1 tag that says Legislation', () => {
        return load_promise.then(() => {expect(component.find('h1').at(0).text()).to.equal("Legislation")})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should have a h4 tag containing the page number', () => {
        return load_promise.then(() => {expect(component.find('h4').length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render a table', () => {
        return load_promise.then(() => {expect(component.find('table').length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render a Row', () => {
        return load_promise.then(() => {expect(component.find(Row).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render a SearchBox', () => {
        return load_promise.then(() => {expect(component.find('form').length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });

    it('should render have Pagination', () => {
        return load_promise.then(() => {expect(component.find(ModelPagination).length).to.equal(1)})
        .catch((err) => {
            assert.fail(err);
        })
    });
});
